#!/usr/bin/env node

require('wcpp').sync()

// Require our modules
const addTwo = require('./addTwo.cpp')
const factorial = require('./factorial.cpp')
const prime = require('./prime.cpp')

// Sumple math
console.log('2 + 3 = ' + addTwo(2, 3))
console.log('10! = ' + factorial(10))

// Primes
console.log('First 20 Primes:')
let primeNumber = 0
for (let i = 0; i < 20; i++) {
  primeNumber = prime.nextPrime(primeNumber)
  console.log(`${primeNumber}`)
}
