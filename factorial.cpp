export int module(int n) {
  if (n > 1)
    return n * module(n - 1);
  else
    return 1;
}
