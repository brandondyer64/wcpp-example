export bool isPrime(int number) {
  if (number == 2 || number == 5) return true;
  // Check if number is divisible by 2 or 5
  if (number % 2 == 0) return false;
  if (number % 5 == 0) return false;

  // Step through odd numbers
  for(int i = 3; i <= number / 2; i += 2) {
    if(number % i == 0) {
      return false;
      break;
    }
  }

  // Number is prime
  return true;
}

export int nextPrime(int prime) {
  while (!isPrime(++prime)) {}
  return prime;
}
